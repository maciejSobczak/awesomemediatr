﻿using MediatR;
using Microsoft.Extensions.Logging;

namespace AwesomeMediatR.Behaviours
{
    public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> 
            where TRequest : IRequest<TResponse>
    {
        private readonly ILogger<LoggingBehavior<TRequest, TResponse>> _logger; 

        public LoggingBehavior(ILogger<LoggingBehavior<TRequest, TResponse>> logger)
        {
            _logger = logger;  
        }

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Handling request of type {typeof(TRequest)}");

            var result = await next();

            _logger.LogInformation($"Handling response of type {typeof(TResponse)}");

            return result;
        }
    }
}
