﻿using Domain.Entities;
using MediatR;

namespace Application.Products.Notifications
{
    public record ProductAddedNotification(Product Product) : INotification;
}
