﻿using FluentValidation;

namespace Application.Products.Commands.UpdateProduct
{
    public class UpdateProductCommandValidator : AbstractValidator<UpdateProductCommand>
    {
        public UpdateProductCommandValidator()
        {
            RuleFor(x => x.Product.Id).NotEmpty();
            RuleFor(x => x.Product.Name).NotEmpty().MaximumLength(100);
        }
    }
}
