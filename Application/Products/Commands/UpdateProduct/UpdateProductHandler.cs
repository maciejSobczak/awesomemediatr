﻿using MediatR;
using Persistence;

namespace Application.Products.Commands.UpdateProduct
{
    public class UpdateProductHandler : IRequestHandler<UpdateProductCommand, Unit>
    {
        private readonly FakeDataStore dataStore;

        public UpdateProductHandler(FakeDataStore dataStore)
        {
            this.dataStore = dataStore;
        }

        public async Task<Unit> Handle(UpdateProductCommand command, CancellationToken cancellationToken)
        {
            await dataStore.UpdateProduct(command.Product);

            return Unit.Value;
        }
    }
}
