﻿using Domain.Entities;
using MediatR;

namespace Application.Products.Commands.UpdateProduct
{
    public record UpdateProductCommand(Product Product) : IRequest;
}
