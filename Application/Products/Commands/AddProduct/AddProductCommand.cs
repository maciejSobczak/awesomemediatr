﻿using Domain.Entities;
using MediatR;

namespace Application.Products.Commands.AddProduct
{
    public record AddProductCommand(Product Product) : IRequest;
}
