﻿using MediatR;
using Persistence;

namespace Application.Products.Commands.AddProduct
{
    public class AddProductHandler : IRequestHandler<AddProductCommand, Unit>
    {
        private readonly FakeDataStore fakeDataStore;

        public AddProductHandler(FakeDataStore fakeDataStore)
        {
            this.fakeDataStore = fakeDataStore;
        }

        public async Task<Unit> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            await fakeDataStore.AddProduct(request.Product);

            return Unit.Value;
        }
    }
}
