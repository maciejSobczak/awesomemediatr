﻿using MediatR;
using Persistence;
using Domain.Entities;

namespace Application.Products.Queries.GetProducts
{
    public class GetProductsHandler : IRequestHandler<GetProductsQuery, IEnumerable<Product>>
    {
        private readonly FakeDataStore fakeDataStore;

        public GetProductsHandler(FakeDataStore dataStore) => fakeDataStore = dataStore;

        public async Task<IEnumerable<Product>> Handle(GetProductsQuery request, CancellationToken cancellationToken)
                => await fakeDataStore.GetAllProducts();
    }
}
