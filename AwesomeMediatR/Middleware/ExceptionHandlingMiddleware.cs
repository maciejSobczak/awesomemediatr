﻿using FluentValidation;
using System.Text.Json;

namespace AwesomeMediatR.Middleware
{
    internal sealed class ExceptionHandlingMiddleware : IMiddleware
    {
        private readonly ILogger<ExceptionHandlingMiddleware> logger;

        public ExceptionHandlingMiddleware(ILogger<ExceptionHandlingMiddleware> logger)
        {
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception e)
            {
                logger.LogError(e, e.Message);

                await HandleExceptionAsync(context, e);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var statusCode = GetStatusCode(exception);
            var response = new
            {
                name = GetName(exception),
                status = statusCode,
                detail = exception.Message,
                errors = GetErrors(exception)
            };

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;

            await context.Response.WriteAsync(JsonSerializer.Serialize(response));
        }

        private int GetStatusCode(Exception ex) =>
            ex switch
            {
                BadHttpRequestException badHttpRequestException => StatusCodes.Status400BadRequest,
                DirectoryNotFoundException directoryNotFoundException => StatusCodes.Status404NotFound,
                ValidationException validationException => StatusCodes.Status422UnprocessableEntity,
                _ => StatusCodes.Status500InternalServerError
            };

        private string GetName(Exception ex) =>
            ex switch
            {
                ApplicationException appex => appex.GetType().FullName,
                _ => "Server error :c"
            };

        private IEnumerable<char> GetErrors(Exception ex)
        {
            var errors = new List<char>();

            if (ex is ValidationException vex)
            {
                errors = vex.Errors.SelectMany(x => x.ErrorMessage).ToList();
            }

            return errors;
        }

    }
}
