using AwesomeMediatR.Behaviours;
using AwesomeMediatR.Middleware;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Persistence;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Text.Encodings.Web;

const string LocalScheme = "local";
const string VisitorScheme = "visitor";
const string PatreonCookieScheme = "patreon-cookie";
const string ParteonScheme = "external-patreon";

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(x =>
{
    x.SwaggerDoc("v1", new OpenApiInfo { Title = "Awesome MediatR", Version = "v1" });
});

builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    var connectionString = builder.Configuration.GetConnectionString("Database");
    options.UseSqlServer(connectionString);
});
var applicationAssembly = typeof(Application.AssemblyReference).Assembly;
builder.Services.AddMediatR(applicationAssembly);
builder.Services.AddValidatorsFromAssembly(applicationAssembly);
builder.Services.AddSingleton<FakeDataStore>();
//builder.Services.AddSingleton(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));
builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
builder.Services.AddTransient<ExceptionHandlingMiddleware>();

builder.Services.AddAuthentication()
    .AddScheme<CookieAuthenticationOptions, VisitorAuthHandler>(VisitorScheme, o => { })
    .AddCookie(LocalScheme)
    .AddCookie(PatreonCookieScheme)
    .AddOAuth(ParteonScheme, o =>
    {
        o.ClientId = "id";
        o.ClientSecret = "password";
        o.SignInScheme = PatreonCookieScheme;

        o.AuthorizationEndpoint = "https://oauth.mocklab.io/oauth/authorize";
        o.TokenEndpoint = "https://oauth.mocklab.io/oauth/token";
        o.UserInformationEndpoint = "https://oauth.mocklab.io/userinfo";

        o.CallbackPath = "/";
        o.Scope.Add("profile");
        o.SaveTokens = true;
    });


builder.Services.AddAuthorization(builder =>
{
    builder.AddPolicy("customer", pb =>
    {
        pb.RequireAuthenticatedUser()
        .AddAuthenticationSchemes(PatreonCookieScheme, LocalScheme, VisitorScheme);
    });
    builder.AddPolicy("user", pb =>
    {
        pb.RequireAuthenticatedUser()
        .AddAuthenticationSchemes("local");
    });
});

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.MapGet("/home", ctx => Task.FromResult("Hello World")).RequireAuthorization("customer");

app.MapGet("/username", (HttpContext context) =>
{
    return context.User.FindFirst("usr")?.Value ?? "empty";
}).RequireAuthorization("user");


app.MapGet("/login-local", async ctx =>
{
    var claims = new List<Claim>
    {
        new Claim("usr", "maciej")
    };
    var identity = new ClaimsIdentity(claims, LocalScheme);
    var user = new ClaimsPrincipal(identity);

    await ctx.SignInAsync(LocalScheme, user);
}).AllowAnonymous();

app.MapGet("/login-patreon", async ctx => await ctx.ChallengeAsync(ParteonScheme, new AuthenticationProperties
{
    RedirectUri = "/home"
})).RequireAuthorization("user");

app.MapGet("/browser", (HttpContext ctx) =>
{
    string browserDetails = string.Empty;
    try
    {
        var browser = ctx.Request.Browser;
        browserDetails =
        "Name = " + browser.Browser + "," +
        "Type = " + browser.Type + ","
        + "Version = " + browser.Version + ","
        + "Major Version = " + browser.MajorVersion + ","
        + "Minor Version = " + browser.MinorVersion + ","
        + "Platform = " + browser.Platform + ","
        + "Is Beta = " + browser.Beta + ","
        + "Is Crawler = " + browser.Crawler + ","
        + "Is AOL = " + browser.AOL + ","
        + "Is Win16 = " + browser.Win16 + ","
        + "Is Win32 = " + browser.Win32 + ","
        + "Supports Frames = " + browser.Frames + ","
        + "Supports Tables = " + browser.Tables + ","
        + "Supports Cookies = " + browser.Cookies + ","
        + "Supports VBScript = " + browser.VBScript + ","
        + "Supports JavaScript = " + "," +
        browser.EcmaScriptVersion.ToString() + ","
        + "Supports Java Applets = " + browser.JavaApplets + ","
        + "Supports ActiveX Controls = " + browser.ActiveXControls
        + ","
        + "Supports JavaScript Version = " +
        browser["JavaScriptVersion"];
        return browserDetails;
    }
    catch (Exception ex)
    {
        return null;
    }
}).AllowAnonymous();

app.MapControllers();

app.Run();

public class VisitorAuthHandler : CookieAuthenticationHandler
{
    public VisitorAuthHandler(IOptionsMonitor<CookieAuthenticationOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
    {
    }

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        var result = await base.HandleAuthenticateAsync();
        if (result.Succeeded)
        {
            return result;
        }

        var claims = new List<Claim>
            {
                new Claim("usr", "maciej")
            };
        var identity = new ClaimsIdentity(claims, "visitor");
        var user = new ClaimsPrincipal(identity);

        await Context.SignInAsync("visitor", user);

        return AuthenticateResult.Success(new AuthenticationTicket(user, "visitor"));
    }
}