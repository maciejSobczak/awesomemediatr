﻿using Application.Products.Notifications;
using MediatR;
using Persistence;

namespace AwesomeMediatR.Handlers
{
    public class EmailHandler : INotificationHandler<ProductAddedNotification>
    {
        private readonly FakeDataStore fakeDataStore;

        public EmailHandler(FakeDataStore fakeDataStore)
        {
            this.fakeDataStore = fakeDataStore;
        }

        public async Task Handle(ProductAddedNotification notification, CancellationToken cancellationToken)
        {
            await fakeDataStore.UpdateOnEvent(notification.Product, "Email sent");
            await Task.CompletedTask;
        }
    }
}
