﻿using Application.Products.Notifications;
using MediatR;
using Persistence;

namespace AwesomeMediatR.Handlers
{
    public class CacheInvalidationHandler : INotificationHandler<ProductAddedNotification>
    {
        private readonly FakeDataStore fakeDataStore;

        public CacheInvalidationHandler(FakeDataStore fakeDataStore)
        {
            this.fakeDataStore = fakeDataStore;
        }

        public async Task Handle(ProductAddedNotification notification, CancellationToken cancellationToken)
        {
            await fakeDataStore.UpdateOnEvent(notification.Product, "Cache invalidated");
            await Task.CompletedTask;
        }
    }
}
