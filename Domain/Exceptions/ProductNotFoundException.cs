﻿namespace Domain.Exceptions
{
    public sealed class ProductNotFoundException : NotFoundException
    {
        public ProductNotFoundException(int productId)
            : base($"The product with identifier {productId} was not found.")
        {
        }
    }
}
