﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IProductRepository
    {
        Task<Product> GetById(int id, CancellationToken cancellationToken = default);

        Task<List<Product>> GetAll(CancellationToken cancellationToken = default);
        
        void Insert(Product product);
        
        void Update(Product product);
    }
}
