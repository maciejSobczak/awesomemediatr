﻿using Domain.Entities;

namespace Persistence
{
    public class FakeDataStore
    {
        private static List<Product> products;

        public FakeDataStore()
        {
            products = new List<Product>
            {
                new Product { Id = 1, Name = "Test Product 1" },
                new Product { Id = 2, Name = "Test Product 2" },
                new Product { Id = 3, Name = "Test Product 3" }
            };
        }

        public async Task AddProduct(Product product)
        {
            products.Add(product);
            await Task.CompletedTask;
        }

        public async Task<IEnumerable<Product>> GetAllProducts() => await Task.FromResult(products);

        public async Task<Product> GetProduct(int id)
        {
            var product = products.FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(product);
        }

        public async Task UpdateProduct(Product product)
        {
            var foundProduct = await GetProduct(product.Id);
            if (foundProduct != null)
            {
                products.Single(p => p.Id == foundProduct.Id).Name = product.Name;
            }
            else
            {
                throw new Exception($"Product woth Id:{product.Id} not found.");
            }
            await Task.CompletedTask;
        }

        public async Task UpdateOnEvent(Product product, string evt)
        {
            products.Single(p => p.Id == product.Id).Name = $"{product.Name} event: {evt}";
            await Task.CompletedTask;
        }
    }
}
