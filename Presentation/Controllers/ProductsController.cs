﻿using Application.Products.Queries.GetProducts;
using Application.Products.Commands.AddProduct;
using Application.Products.Commands.UpdateProduct;
using Application.Products.Notifications;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IMediator mediator;

        public ProductsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult> GetProducts()
        {
            var products = await mediator.Send(new GetProductsQuery());

            return Ok(products);
        }

        [HttpPost]
        public async Task<ActionResult> AddProduct([FromBody] Product product)
        {
            await mediator.Send(new AddProductCommand(product));

            await mediator.Publish(new ProductAddedNotification(product));

            return Ok();
        }

        [HttpPut("{productId:int}")]
        public async Task<IActionResult> UpdateProduct(int productId, [FromBody] UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var command = request;
            command.Product.Id = productId;

            await mediator.Send(command, cancellationToken);

            return new NoContentResult();
        }

    }
}
